# Andrew J. Danelz 
linktr.ee/vordimous

## OBJECTIVE:

I am a full-stack engineer and developer advocate passionate about cloud application design and implementation. That is accomplished by diligent design and programming with a strong focus on DevOps and orchestration. I will contribute to the success of a software project by being a strong proponent of these ideals and advocating internally and externally for the developer community.

## TECHNICAL SKILLS:

Backend | UI | Data | CI/CD
--- | --- | --- | ---
Golang | JS, Vue, React | PostgreSQL, Hibernate, Elastic Search | Git, Docker
Java, Spring Boot |  RabbitMQ, WebSockets | Kafka, Protobuf | Kubernetes, Helm 

## RELEVANT WORK EXPERIENCE:

**Head of Developer Relations** *WSO2, REMOTE* | Jan 2022 - Present

I lead the team that builds and fosters the community of developers around the Open Source and SAAS projects at WSO2 along with the Ballerina programming language (ballerina.io). Additionally, I led the technical writers who worked on each product documentation site and the company training material.

**Lead Solutions Engineer** *WSO2, REMOTE* | Feb 2021 - Jan 2022

I was a technical sales rep that worked with customers to solve their problems with WSO2 products.

**Sr. Full Stack Developer** *BMW: Apps and Services* | Feb 2020 - Feb 2021

I was a senior member of the team building systems to provide electric car drivers with an optimized way to charge their vehicles called bmwchargeforward.com. This app connected many internal BMW systems but interfaced with Power companies for up-to-date energy data. In addition, I coordinated with top vehicle manufacturers and utility companies to build a system to securely send electrical grid data from the utility companies to each electric car manufacturer.

- **BMW Charge Forward**: This project includes a collection of BMW internal services providing vehicle data sources. They are used to optimize electric vehicles charging times. To manage the streams of vehicle event data, AWS Lambda, SQS, and Kinesis were built together in a Typescript mono repo. The backend was built with node Nest.js framework and PostgreSQL.


**Sr. Enterprise Application Engineer** *AFS Logistics* | 2018 - Feb 2020

I worked as a lead engineer on a team building a new Transportation Management System from the ground up using a microservice architecture. Spring Boot, gRPC, Protobuf, Kafka, Elastic Search, Vue.js, PostgreSQL, Docker, Kubernetes, Helm.

- **Transportation Management System** These are the projects that I participate in:
    - A CRUD application that is the main entry point into the system. It is built using `Spring Boot, MS SQL Server, and Vue`.
    - A Rating System designed for high throughput and consists of multiple microservices using `Spring Boot, Golang, Protobuf, GRPC, Redis, PostgreSQL, Vue`.
    - A Document Manager built to control the ingestion of physical documents using `Spring Boot, Protobuf, GRPC, Websockets, RabbitMQ, PostgreSQL, Google Cloud API, and Vue`.
    - A Delayed Messaged Delivery service that can scale for high volume is built with `Kafka, Redis, and Spring Boot`.
    - General tools/skills: GitLab, JIRA, Slack, Google Cloud

**Full Stack Mentor** *Thinkful.com, REMOTE* | January 2018 to 2020

- I work one on one with students participating in the program. My main goal is to help overcome hurdles and provide more depth to the program where needed based on my work experience. The program teaches full stack web development using the `MERN` stack.
- General tools/skills: Slack, Group Video Chat, Bill.com Invoicing

**Developer/Coder/Tinker** *Myself* | Always and Forever

- I wrote an `open source` tool to reproduce the Kafka delayed/scheduled message system using `Golang` [Gohlay](https://gitlab.com/vordimous/gohlay) WIP. It has a much smaller footprint and can scale more easily without the dependency on Redis.
- I build and maintain my wife's business website [Heart Strings Music Therapy Services, LLC](https://heartstringsmts.com/) with #1 google SEO ranking for "Music Therapy Upstate SC"

**Sr. Enterprise Application Developer** *Transplace.com* | October 2012 to December 2017
I maintained legacy applications, worked on improvements and new features of existing products, and architected new applications.

- **EDI parser**: This reliable `EDI` parser is able to convert any EDI file into an object format of choice (XML, JSON, etc.). I developed the algorithm that utilized specific Java class definitions to know how to parse any type of EDI file. This gave us the power to define broad or specific rules in each `Java` class file that would "teach" the algorithm how to read any EDI file. The classes could then be extended to build custom file parsers.
- **Transportation Management System**: This is a monolith `Java` application that I helped maintain and improve. I architected multiple aspects of the project, from database models to application service layer using `Spring Boot` and `PostgreSQL`. I enhanced application performance by implementing `Elasticsearch` clusters for dynamic searching on live data. Updated legacy apps to a modern web UI with `Vue.js`. Build APIs on top of Legacy services and add microservices to facilitate CI/CD.
- **Cloud Migration**: I worked closely with the `DevOps` team to rework all existing app configurations to be deployable in Docker containers. This allowed us to build and deploy any version of any app together to create any type of environment we needed. It was also beneficial for local development, where each dev could learn about and run our entire application stack locally with minimal effort. These containers were then deployed to VM clusters using a collection of `Ansible` scripts.
- **Headphones.js**: I Created a custom framework on top of `Knockout.js`. This included common js libs as well as a model and state management library that I wrote, allowing developers to define front-end objects. I built a collection of front-end widgets for this framework to standardize the UX in our web apps. The framework was compiled and deployed to a CDN using `Gulp`.

## OTHER ANECDOTES:

Bachelor of Science in Computer Science, _Southern Wesleyan University_, Cum Laude

Research article: Path Planning using Dijkstra and Lightning Enhancement

Eagle Scout with the Silver Palm